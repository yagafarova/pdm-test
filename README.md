# pdm-test

[![Release](https://img.shields.io/github/v/release/y_ksenia/pdm-test)](https://img.shields.io/github/v/release/y_ksenia/pdm-test)
[![Build status](https://img.shields.io/github/actions/workflow/status/y_ksenia/pdm-test/main.yml?branch=main)](https://github.com/y_ksenia/pdm-test/actions/workflows/main.yml?query=branch%3Amain)
[![codecov](https://codecov.io/gh/y_ksenia/pdm-test/branch/main/graph/badge.svg)](https://codecov.io/gh/y_ksenia/pdm-test)
[![Commit activity](https://img.shields.io/github/commit-activity/m/y_ksenia/pdm-test)](https://img.shields.io/github/commit-activity/m/y_ksenia/pdm-test)
[![License](https://img.shields.io/github/license/y_ksenia/pdm-test)](https://img.shields.io/github/license/y_ksenia/pdm-test)

Testing of pdm template

- **Github repository**: <https://github.com/y_ksenia/pdm-test/>
- **Documentation** <https://y_ksenia.github.io/pdm-test/>

## Getting started with your project

First, create a repository on GitHub with the same name as this project, and then run the following commands:

``` bash
git init -b main
git add .
git commit -m "init commit"
git remote add origin git@github.com:y_ksenia/pdm-test.git
git push -u origin main
```

Finally, install the environment and the pre-commit hooks with

```bash
make install
```

You are now ready to start development on your project! The CI/CD
pipeline will be triggered when you open a pull request, merge to main,
or when you create a new release.

To finalize the set-up for publishing to PyPi or Artifactory, see
[here](https://fpgmaas.github.io/cookiecutter-pdm/features/publishing/#set-up-for-pypi).
For activating the automatic documentation with MkDocs, see
[here](https://fpgmaas.github.io/cookiecutter-pdm/features/mkdocs/#enabling-the-documentation-on-github).
To enable the code coverage reports, see [here](https://fpgmaas.github.io/cookiecutter-pdm/features/codecov/).

## Releasing a new version

- Create an API Token on [Pypi](https://pypi.org/).
- Add the API Token to your projects secrets with the name `PYPI_TOKEN` by visiting
[this page](https://github.com/y_ksenia/pdm-test/settings/secrets/actions/new).
- Create a [new release](https://github.com/y_ksenia/pdm-test/releases/new) on Github.
Create a new tag in the form ``*.*.*``.

For more details, see [here](https://fpgmaas.github.io/cookiecutter-pdm/features/cicd/#how-to-trigger-a-release).
